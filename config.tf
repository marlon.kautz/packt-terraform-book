terraform {
  backend "s3" {
    bucket = "terraform-exercise"
    key = "mighty_trousers/terraform.tfstate"
    region = "us-east-1"
    dynamodb_table = "terraform-exercise"
  }
}